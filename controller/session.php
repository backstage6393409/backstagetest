<?php
/**
 * Created by SmashingTeam
 * User: ivashkin
 * Date: 2019-03-12
 * Time: 16:35
 */

namespace App\Core\Session;

use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Support\Arr;

class DbSessionHandler extends DatabaseSessionHandler
{
    /**
     * Get the default payload for the session.
     *
     * @param string $data
     * @return array
     */
    protected function getDefaultPayload($data)
    {
        $payload = [
            'payload'       => base64_encode($data),
            'last_activity' => $this->currentTime(),
        ];

        $this->addAdditionalInformation($payload, $data);

        if (!$this->container) {
            return $payload;
        }

        return tap($payload, function (&$payload) {
            $this->addUserInformation($payload)
                ->addRequestInformation($payload);
        });
    }

    private function addAdditionalInformation(array &$payload, string $data)
    {
        $data = unserialize($data);

        if ($clientCode = Arr::get($data, 'clientCode', '')) {
            $payload['clientCode'] = $clientCode;
        }

        if (count(Arr::get($data, 'cart.products', []))) {
            // if cart not empty
            $weight = 3;
        } elseif (Arr::get($data, 'adwords', 0)) {
            // if user come from adwords
            $weight = 2;
        } elseif (Arr::get($data, 'grmk', 0)) {
            // if user come from remarketing
            $weight = 1;
        } elseif (Arr::get($data, 'gclid', 0)) {
            // if user come from gclid
            $weight = 4;
        } else {
            // if cart is empty
            $weight = 0;
        }

        $payload['weight'] = $weight;
    }
}

