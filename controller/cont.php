<?php

namespace App\Http\Controllers;

use App\Services\Helpers\CategoriesParams;
use App\Services\MixCatalog\CatalogTypes\Synthetic\Catalog;
use App\Services\Products\Faq\ConfirmMail as FaqService;
use App\Services\Products\Review\ConfirmMail as ReviewService;
use App\Services\MixCatalog\Params as MixCatalogParams;

class ConfirmController extends Controller
{
    public function __construct(private Catalog $syntheticCatalog)
    {
    }

    public function faq($hash, FaqService $faqService)
    {
        $faqService->success($hash);

        return view('faq.confirm_email', $this->getDataView());
    }

    public function review($hash, ReviewService $reviewService)
    {
        $reviewService->success($hash);

        return view('review.confirm_email', $this->getDataView());
    }

    private function getDataView(): array
    {
        return [
            'parentsCategories' => $this->syntheticCatalog->getParentCategories((new CategoriesParams())),
            'childCategories'   => $this->syntheticCatalog->getChildrenCategories(
                (new CategoriesParams())->setParamRating(MixCatalogParams::TOP_CATEGORY_RATING)
                    ->setLimitChildren(12)
            ),
        ];
    }
}

